import pygame
import numpy as np
import random
import time

#GLOBAL VARIABLES
roomDimensions = (10,8)
nPersons = 5 #maximum number of persons is 6
sleep = 0 #add sleep time between stages of the simulation

class Person:
    def __init__(self, room, ID):
        self.ID = ID
        self.room = room
        self.pos = self.startingPosition()
        self.interacted = False
        self.moved = False
        self.partner = None
        self.hasPartner = False
        #traits
        self.friendliness = random.randint(1,10)
        self.selfworth = random.randint(1,10)
        #stats
        self.happiness = 0
        self.loneliness = 0

    def startingPosition(self):
        poses = [(i,j) for i in range(self.room.dim[0]) for j in range(self.room.dim[1])]
        self.pos = random.choice(list(filter(self.room.isEmpty,poses)))
        self.room.map[self.pos] = self.ID
        return self.pos

    def move(self):
        adj = [tuple(np.array(self.pos) + np.array(i)) for i in Room.cross] 
        adjInRoom = list(filter(self.room.isInRoom,adj))
        adjEmpty = list(filter(self.room.isEmpty,adjInRoom))
        if len(adjEmpty):
            newPos = random.choice(adjEmpty)
        else:
            newPos = self.pos
        self.loneliness += 0.1
        self.room.map[self.pos] = 0
        self.pos = newPos
        self.room.map[self.pos] = self.ID
 
    def getPartner(self):
        adjOccupied = self.room.adjOccupied(self.pos)
        neighborsID = [int(self.room.map[i]) for i in adjOccupied] #get values (ID) of occupied places
        if not len(neighborsID):
            self.partner = None
        else:
            neighborsID.sort()
            for i, neighbor in enumerate(neighborsID):
                partnerID = neighborsID[i]
                partner = self.room.persons[partnerID-1]
                if (partner.partner and partner.partner != self):
                    #neighbor already has a partner
                    self.partner = None
                elif partner.interacted:
                    #neighbor has already interacted
                    self.partner = None
                else:
                    #partner available
                    self.partner = partner
                    partner.partner = self
                    break

    def interact(self, partner):
        #print(f"Person {self.ID} performed INTERACTION with Person {self.partner.ID}::: {self.ID} <> {self.partner.ID}") 
        if self.partner.interacted:
            if self.partner.interacted == 1 or self.partner.interacted == 3:
                self.chat()
            elif self.partner.interacted == 2:
                self.insult()

        else:
            discr = self.friendliness + self.partner.friendliness + random.randint(0,10)
            if discr > 15:
                self.chat()
            else:
                self.insult()
            #add a biais depending on mood
        #add other actions depending on person stats

    def chat(self):
        self.happiness += 1
        self.interacted = 1

        discr = self.happiness + self.partner.friendliness * 3 + random.randint(0,10)
        if discr > 20:
            self.gift()

    def insult(self):
        self.happiness -= 1
        self.interacted = 2

    def gift(self):
        self.happiness += 1
        self.partner.selfworth += 1
        self.interacted = 3

    def step(self):
        if not self.interacted:
            if self.partner:
                self.interact(self.partner)
            else:
                self.getPartner()
                if not self.partner:
                    #found no partner
                    self.moved = True
                elif not self.partner.partner or self.partner.partner == self:
                    self.interact(self.partner)
                else:
                    self.moved = True

        else:
            self.moved = True

class Room:
    cross = [(0,1),(0,-1),(1,0),(-1,0)]

    def __init__(self,dim):
        self.dim = dim
        self.map = np.zeros(dim)
        self.persons = {}
        for i in range(nPersons):
            self.persons[i] = Person(self,i+1) 

    def isOccupied(self,pos):
        return self.isInRoom(pos) and self.map[pos]

    def isEmpty(self, pos):
        return self.isInRoom(pos) and not self.map[pos]

    def isInRoom(self,pos):
        return 0 <= pos[0] < self.dim[0] and 0 <= pos[1] < self.dim[1]

    def adjOccupied(self,pos):
        adj = [tuple(np.array(pos) + np.array(i)) for i in Room.cross] 
        adjOccupied = list(filter(self.isOccupied,adj))
        return adjOccupied

    def arePeopleAround(self,pos):
        if len(self.adjOccupied(pos)): return True

    def step(self):
        #print("STEP")
        for i, person in self.persons.items():
            person.step()
                    
        for i, person in self.persons.items():
            person.partner = None
            if person.moved:
                person.move()
                person.moved = False
                person.interacted = False

class Graphic:
    facesFiles = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg"]
    actionsFiles = ["chat.jpg", "insult.jpg", "gift.jpg"]

    faces = []
    actions = []

    def loadFaces(unit):
        Graphic.faces = [None] * len(Graphic.facesFiles)
        for i, face in enumerate(Graphic.facesFiles):
            Graphic.faces[i] = pygame.transform.scale(pygame.image.load(face), (unit,unit))

    def loadActions(unit):
        Graphic.actions = [None] * len(Graphic.actionsFiles)
        for i, action in enumerate(Graphic.actionsFiles):
            Graphic.actions[i] = pygame.transform.scale(pygame.image.load(action), (unit//2, unit//2))

class Simul:

    def __init__(self, room, nPersons, unit, roomDimensions, textSpace, sleep):
        self.room = room
        self.nPersons = nPersons
        self.unit = unit
        self.textSpace = textSpace
        self.screen = pygame.display.set_mode([(roomDimensions[0] + self.textSpace) * unit, roomDimensions[1] * unit]) 
        self.font = pygame.font.Font("freesansbold.ttf", 16)
        self.vertical = roomDimensions[1] / nPersons * unit
        self.textDir = np.array([0,self.vertical])
        self.textOrigin = np.array([roomDimensions[0] * unit, -self.vertical + 10])
        Graphic.loadFaces(unit)
        Graphic.loadActions(unit)
 
    def faces(self):
        for ID in range(self.nPersons): 
            pos = self.room.persons[ID].pos 
            coords = (pos[0] * unit, pos[1] * unit) 
            self.screen.blit(Graphic.faces[ID], coords)
            
    def actions(self):
        for i, person in self.room.persons.items():
            if person.interacted:
                pos = person.pos
                coords = (pos[0] * unit + unit//3, pos[1] * unit - unit//3)
                self.screen.blit(Graphic.actions[person.interacted - 1], coords)

    def displayStats(self):
        for ID, person in self.room.persons.items():
            string1 = f"Friendliness = {person.friendliness}      Self-worth = {person.selfworth}"
            string2 = f"Happiness = {person.happiness}      Loneliness = {int(np.floor(person.loneliness))}"
            traitsFont = self.font.render(string1, True, (70,70,70))
            statsFont = self.font.render(string2, True, (0,0,0))
            face = pygame.transform.scale(Graphic.faces[ID], (unit//2, unit//2))
            facePos = self.textOrigin + self.textDir * person.ID
            traitsPos = facePos + np.array([unit//2 + 10,1])
            statsPos = traitsPos + np.array([0,unit//3])
            self.screen.blit(face, facePos)
            self.screen.blit(traitsFont, traitsPos)
            self.screen.blit(statsFont, statsPos)
 
    def draw(self):
        pygame.display.set_caption("People interactions")
        self.screen.fill((255,255,255))
        self.faces()
        self.actions()
        self.displayStats()
        pygame.display.flip()

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
            self.room.step()
            self.draw()
            time.sleep(sleep)

    def quit(self):
        pygame.quit()

if __name__ == "__main__":
    pygame.init()
    screenMax = (1200,1000)
    textSpace = 6
    ratio = (screenMax[0] // (roomDimensions[0] + textSpace), screenMax[1] // roomDimensions[1])
    unit = min(ratio)
    room = Room(roomDimensions)
    simul = Simul(room,nPersons,unit,roomDimensions,textSpace,sleep)
    simul.run()
