import board
import numpy as np

room = board.Board((8,4))

room[0,0] = "Bob"
room[7,3] = "Alice"

roomposes = np.array((np.zeros(32).reshape((8,4))))

print(roomposes)
roomposesAsIs = roomposes.reshape(32)
print(roomposesAsIs)

roomposes[(0,1)] = 1
print(roomposes)
